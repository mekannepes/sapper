let width;
let height;
let amountOfBombs;

let isWidthError = true;
let isHeightError = true;
let isBombError = true;

let widthError;
let heightError;
let amountBombsError;

let inputWidth = 10
let inputHeight = 10
let inputBombs = 10

document.addEventListener("DOMContentLoaded", function(event) {
    Start()
});


let HEIGHT;
let WIDTH ;
let AVAILABLE_FLAGS;

let valueArray = [];

let bombArray = [];
let flagsArray = [];

let startPoint;

let sellSize;
let fontSize;

function Start() {

    WIDTH = +inputWidth;
    HEIGHT = +inputHeight;
    AVAILABLE_FLAGS = +inputBombs;

    bombArray = [];
    valueArray = [];
    flagsArray = [];


    sellSize =  '' + (80 - ((HEIGHT - 10) * 4)) + 'px';
    fontSize =  '' + (20 - (HEIGHT - 10)) + 'px';

    let block = document.getElementById('root')

    while(block.firstChild) {
        block.firstChild.remove();
    }

    let playground = document.createElement('div');
    playground.setAttribute('id', 'playground');

    let table = document.createElement('table');
    table.setAttribute('id', 'sapperField');
    table.className = "table table-bordered mt-3 mb-3 ";

    for (let y = 0; y < HEIGHT; y++) {
        let row = document.createElement('tr');
        for (let x = 0; x < WIDTH; x++) {
            let cell = document.createElement('td');
            cell.classList.add("cell", "align-middle", "closed-cell");
            cell.style.width = sellSize;
            cell.style.height = sellSize;
            cell.style.fontSize = fontSize;
            cell.style.borderColor = "#000000"

            cell.setAttribute('id', 'row' + y + 'col' + x);
            cell.onclick = () => {
                startPoint = new Kletka(x, y);
                startPoint.PositionStatus(WIDTH, HEIGHT);
                renderField()
            };

            row.append(cell);
        }
        table.append(row);
    }

    let amount = document.createElement('p');
    amount.innerHTML = AVAILABLE_FLAGS - flagsArray.length;


    let button = document.createElement('input');
    button.classList.add('btn', 'btn-info', 'mb-3');
    button.setAttribute('value', 'Restart');
    button.setAttribute('type', 'button');
    button.onclick = restart;

    playground.append(table);
    playground.append(button);

    block.append(playground);

    document.body.append(block);
}

function renderField() {

    for (let y = 0; y < HEIGHT; y++) {
        for(let x = 0; x < WIDTH; x++) {
            let cell = new Kletka(x, y);
            cell.PositionStatus(WIDTH, HEIGHT);
            valueArray.push(cell);
        }
    }

    generateBombPlaces(0, (WIDTH * HEIGHT) - 1);

    for(let i = 0; i < bombArray.length; i++){
        valueArray[bombArray[i]].value = "B";
    }

    for(let i = 0; i < (WIDTH * HEIGHT); i++) {
        if(!valueArray[i].isBomb()) {
            valueArray[i].Value(bombArray);
        }
    }

    for(let y = 0; y < HEIGHT; y++) {
        for(let x = 0; x < WIDTH; x++) {

            let cell = document.getElementById('row' + y + 'col' + x);

            let currentPosition = y*WIDTH + x;

            if(valueArray[currentPosition].isEmpty()) {
                cell.onclick = (() => {
                    openCells(x, y);
                })
            } else if (valueArray[currentPosition].isBomb()) {
                cell.onclick = (() => {
                    clickOnBomb();
                })
            } else {
                cell.onclick = (() => {
                    showCell(x, y);
                });
            }
            cell.oncontextmenu = (() => {
                putFlag(x, y);
            })
        }
    }

    openCells(startPoint.X, startPoint.Y);
}

function generateBombPlaces(min, max) {

    for(let i = 0; i < AVAILABLE_FLAGS; i++) {
        let rand = min + Math.random() * (max + 1 - min);
        let value = Math.floor(rand);

        if(isNearbyStartPoint(value) || bombArray.includes(value)) {
            i--;
        } else {
            bombArray.push(value);
        }
    }
}

function isNearbyStartPoint(bombPosition) {

    let coordinates = startPoint.solveCurrentPosition();

    if(coordinates === bombPosition) return true;

    if(startPoint.positionStatus.includes('N')) {
        if(coordinates - WIDTH === bombPosition) return true;

        if(startPoint.positionStatus.includes('W')) {
            if(coordinates - (WIDTH + 1) === bombPosition) return true;
        }

        if(startPoint.positionStatus.includes('E')) {
            if(coordinates - (WIDTH - 1) === bombPosition) return true;
        }
    }

    if(startPoint.positionStatus.includes('E')) {
        if(coordinates + 1 === bombPosition) return true;
    }

    if(startPoint.positionStatus.includes('S')) {
        if(coordinates + WIDTH === bombPosition) return true;

        if(startPoint.positionStatus.includes('W')) {
            if(coordinates + (WIDTH - 1) === bombPosition) return true;
        }

        if(startPoint.positionStatus.includes('E')) {
            if(coordinates + (WIDTH + 1) === bombPosition) return true;
        }
    }

    if(startPoint.positionStatus.includes('W')) {
        if(coordinates - 1 === bombPosition) return true;
    }

    return false;
}

function openCells(x, y) {

    let cell = valueArray[y*WIDTH + x];

    if(!cell.isCalled) {

        if(!cell.isBomb()) {
            showCell(x, y)
        }

        if(cell.isEmpty()) {

            if(cell.positionStatus.includes('N')) openCells(x, y-1);

            if(cell.positionStatus.includes('N') && cell.positionStatus.includes('E')) openCells(x+1, y-1);

            if(cell.positionStatus.includes('E')) openCells(x+1, y);

            if(cell.positionStatus.includes('E') && cell.positionStatus.includes('S')) openCells(x+1, y+1);

            if(cell.positionStatus.includes('S')) openCells(x, y+1);

            if(cell.positionStatus.includes('S') && cell.positionStatus.includes('W')) openCells(x-1, y+1);

            if(cell.positionStatus.includes('W')) openCells(x-1, y);

            if(cell.positionStatus.includes('W') && cell.positionStatus.includes('N')) openCells(x-1, y-1);

        }
    }
}

function showCell(x, y) {

    let cell = document.getElementById('row' + y + 'col' + x);
    cell.onclick = null;
    cell.oncontextmenu = null;

    let style = "cell" + valueArray[y*WIDTH + x].value;
    cell.classList.add(style);

    valueArray[y * WIDTH + x].isCalled = true;

    if(!valueArray[y * WIDTH + x].isEmpty() && !valueArray[y*WIDTH + x].isBomb()) {
        clearCell(cell);
        cell.classList.remove('flagged');
        let value = document.createElement('p');
        value.innerHTML = valueArray[y*WIDTH + x].value;
        value.classList.add("mb-0");
        cell.append(value);
    } else if (valueArray[y*WIDTH + x].isBomb()) {
        clearCell(cell);
    }
}

function clearCell(cell) {
    while(cell.firstChild) {
        cell.firstChild.remove();
    }
}

function setIcon(cell, icon, alt) {
    let iconEl = document.createElement('img');
    iconEl.setAttribute('src', icon);
    iconEl.setAttribute('alt', alt);
    iconEl.classList.add('icon');
    cell.append(iconEl);
}

function putFlag(x, y) {
    console.log("GELDI")
    if(flagsArray.length < AVAILABLE_FLAGS ) {
        let cell = document.getElementById('row' + y + 'col' + x);
        cell.classList.add('flagged');

        setIcon(cell, 'images/flag.png', 'flag');

        cell.oncontextmenu = (() => {
            removeFlag(x, y);
        })

        flagsArray.push(y*WIDTH + x);

        changeCounterFlag();

        if(compareFlagsAndBombs()) {
            openAllField();
            setResultLogo('You win');
        }
    }
}

function compareFlagsAndBombs() {
    if(bombArray.length !== flagsArray.length) {
        return false;
    }

    for(let i = 0; i < bombArray.length; i++){
        if(!flagsArray.includes(bombArray[i])) return false;
    }

    return true;
}

function removeFlag(x, y) {
    let cell = document.getElementById('row' + y + 'col' + x);
    cell.classList.remove('flagged');

    clearCell(cell);
    cell.classList.remove('flagged');

    cell.oncontextmenu = (() => {
        putFlag(x, y);
    })
    let index = flagsArray.findIndex(element => element === (y * WIDTH + x));
    flagsArray.splice(index, 1);

    changeCounterFlag();
}

function changeCounterFlag() {
    let counterFlag = document.getElementById('counterFlagBlock');
    counterFlag.lastChild.remove();
    let amount = document.createElement('p');
    amount.innerHTML = AVAILABLE_FLAGS - flagsArray.length;
    counterFlag.append(amount);
}

function clickOnBomb() {

    for(let i = 0; i < bombArray.length; i++){
        let coordinates = bombArray[i];
        let x_coordinate = coordinates % WIDTH;
        let y_coordinate = Math.floor(coordinates/WIDTH);
        showCell(x_coordinate, y_coordinate);
    }

    cleanAllClickListener();

    setResultLogo('You died');
}

function openAllField() {

    for(let y = 0; y < HEIGHT; y++) {
        for(let x = 0; x < WIDTH; x++) {
            let cell = document.getElementById('row' + y + 'col' + x);
            cell.onclick = null;
            cell.oncontextmenu = null;

            if(!valueArray[y*WIDTH + x].isCalled) {
                showCell(x, y);
            }
        }
    }

}

function cleanAllClickListener() {

    for(let y = 0; y < HEIGHT; y++) {
        for(let x = 0; x < WIDTH; x++) {
            let cell = document.getElementById('row' + y + 'col' + x);
            cell.onclick = null;
            cell.oncontextmenu = null;
        }
    }
}

function setResultLogo(alt) {
    let block = document.getElementById('root');
    let result = document.createElement('h1');
    result.classList.add('result-text');
    result.textContent = alt
    result.style.color = "red"
    result.style.fontSize = '130px'
    block.append(result);
}

function restart() {
    Start();
}

class Kletka {

    value;
    X;
    Y;
    positionStatus;
    isCalled;

    constructor(X, Y) {
        this.X = X;
        this.Y = Y;
        this.isCalled = false;
    }

    PositionStatus(WIDTH, HEIGHT) {
        this.positionStatus = '';

        if(this.Y !== 0) {
            this.positionStatus = this.positionStatus + 'N';
        }

        if(this.X !== WIDTH - 1) {
            this.positionStatus = this.positionStatus + 'E';
        }

        if(this.Y !== HEIGHT - 1) {
            this.positionStatus = this.positionStatus + 'S';
        }

        if(this.X !== 0) {
            this.positionStatus = this.positionStatus + 'W';
        }
    }

    Value(bombArray) {

        let counter = 0;
        let currentPosition = this.solveCurrentPosition();

        if(this.positionStatus.includes('N')) {
            if(bombArray.includes(currentPosition - WIDTH)) counter++;

            if(this.positionStatus.includes('W')) {
                if(bombArray.includes(currentPosition - (WIDTH + 1))) counter++;
            }

            if(this.positionStatus.includes('E')) {
                if(bombArray.includes(currentPosition - (WIDTH - 1))) counter++;
            }
        }

        if(this.positionStatus.includes('E')) {
            if(bombArray.includes(currentPosition + 1)) counter++;
        }

        if(this.positionStatus.includes('S')) {
            if(bombArray.includes(currentPosition + WIDTH)) counter++;

            if(this.positionStatus.includes('W')) {
                if(bombArray.includes(currentPosition + (WIDTH - 1))) counter++;
            }

            if(this.positionStatus.includes('E')) {
                if(bombArray.includes(currentPosition + (WIDTH + 1))) counter++;
            }
        }

        if(this.positionStatus.includes('W')) {
            if(bombArray.includes(currentPosition - 1)) counter++;
        }

        this.value = counter;

    }

    solveCurrentPosition() {
        return this.Y * WIDTH + this.X;
    }

    isBomb() {
        return this.value === 'B';
    }

    isEmpty() {
        return this.value === 0;
    }
}
